"""uptwinkles URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from rest_framework import routers
from uptwinkles import views
from django.contrib import admin


router = routers.DefaultRouter()
router.register(r'people', views.PersonViewSet)
router.register(r'chores', views.ChoreViewSet)
router.register(r'meals', views.MealViewSet)
router.register(r'chore-assignments', views.ChoreAssignmentViewSet, base_name='chore-assignments')
router.register(r'dinner-assignments', views.DinnerAssignmentViewSet, base_name='dinner-assignments')
router.register(r'dishes-assignments', views.DishesAssignmentViewSet, base_name='dishes-assignments')
router.register(r'cleanup-assignments', views.CleanupAssignmentViewSet, base_name='cleanup-assignments')
router.register(r'backup-assignments', views.BackupAssignmentViewSet, base_name='backup-assignments')



urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
