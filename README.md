# Uptwinkles API

This is just a database of content shared by Uptwinkles services (like the dashboard and Twink). It's powered by Django and serves a public REST API using Django Rest Framework.

## Local development

    $ mkvirtualenv uptwinkles-api --python=python3
    $ workon uptwinkles-api
    $ pip install -r requirements.txt
    $ python manage.py migrate
    $ python manage.py runserver

## Deploy

Assuming you have access,

Do this the first time:

    $ git remote add dokku dokku@uptwinkles.co:api

Do this to deploy:

    $ git push dokku master

## License

AGPL 3.0+
