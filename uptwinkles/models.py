import requests
from urllib.parse import quote_plus
from django.db import models


class Person(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    active = models.BooleanField(
        default=True,
        help_text="This person is present and able to do assigned chores."
    )
    matrix_id = models.CharField(
        max_length=255,
        help_text="Full Matrix username, in the format of @user:server.tld (@<username>:matrix.org by default)",
        blank=True
    )

    @property
    def avatar_url(self):
        matrix_urlsafe = quote_plus(self.matrix_id)
        url = "https://matrix.org/_matrix/client/r0/profile/{}/avatar_url"
        r = requests.get(url.format(matrix_urlsafe))
        avatar_id = r.json()['avatar_url'][6:] # MAGIC! https://github.com/matrix-org/matrix-python-sdk/blob/3039e32e6be3c1061ab79954312e0be1079ddff6/matrix_client/api.py#L634
        return "https://matrix.org/_matrix/media/r0/download/{}".format(avatar_id)

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    class Meta:
        verbose_name_plural = 'people'


class Chore(models.Model):
    name = models.CharField(
        max_length=255,
        help_text='2nd person imperative with the subject omitted. All lowercase. Ex: "take out the trash"'
    )
    short_name = models.CharField(
        max_length=255,
        help_text="Single word, noun or verb, that best describes this chore."
    )
    image = models.ImageField(upload_to='chores')

    def __str__(self):
        return "[{}] {}".format(self.short_name, self.name)


class Meal(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name
