from os import path
import math
import random
from datetime import datetime, timedelta
from .models import Person, Chore, Meal
from .serializers import PersonSerializer, ChoreSerializer, MealSerializer
from django.conf import settings
from rest_framework.response import Response
import shelve


def get_dinner_today():
    db = shelve.open(path.join(settings.BASE_DIR, 'shelve.db'), writeback=True)

    try:
        dinner = random.choice(Meal.objects.all())
    except:
        dinner = None

    db.setdefault('today', (datetime.now() - datetime(1970,1,1)).days)
    db.setdefault('dinner_today', dinner)

    day = (datetime.now() - datetime(1970,1,1)).days
    if day > db['today']:
        db['dinner_today'] = dinner
        db['today'] = day

    return db['dinner_today']


def chore_assignments(day):
    people = Person.objects.filter(active=True)
    chores = Chore.objects.all()

    # Number of weeks since epoch
    epoch = datetime(1970,1,1)
    offset = math.floor(((day - epoch).days + 3) // 7)

    assignments = []
    if people:
        for n, chore in enumerate(chores):
            index = (n + offset) % len(people)
            person = people[index]
            assignments.append({
                "person": PersonSerializer(person).data,
                "chore": ChoreSerializer(chore).data
            })

    day_assignments = {
        "startDate": epoch + timedelta(days=offset*7-3),
        "endDate": epoch + timedelta(days=(offset+1)*7-3),
        "assignments": assignments
    }

    return day_assignments


def dinner_assignment(day):
    """Returns the dinner assignment (dict) for a given day"""
    day = datetime(year=day.year, month=day.month, day=day.day)
    now = datetime(year=datetime.now().year, month=datetime.now().month, day=datetime.now().day)

    people = Person.objects.filter(active=True)

    # Number of days since epoch
    if people:
        offset = (day - datetime(1970,1,1)).days % len(people)
        person = PersonSerializer(people[offset]).data
    else:
        person = None

    assignment = {
        "date": day,
        "person": person
    }

    if day == now:
        assignment["meal"] = MealSerializer(get_dinner_today()).data

    return assignment


def dishes_assignment(day):
    """Returns the dishes assignment (dict) for a given day"""
    day = datetime(year=day.year, month=day.month, day=day.day)
    people = Person.objects.filter(active=True)

    # Number of days since epoch, + 2
    if people:
        offset = (day - datetime(1970,1,3)).days % len(people)
        person = PersonSerializer(people[offset]).data
    else:
        person = None

    assignment = {
        "date": day,
        "person": person
    }
    return assignment

def cleanup_assignment(day):
    """Returns the cleanup assignment (dict) for a given day"""
    day = datetime(year=day.year, month=day.month, day=day.day)
    people = Person.objects.filter(active=True)

    # Number of days since epoch, + 2
    if people:
        offset = (day - datetime(1970,1,5)).days % len(people)
        person = PersonSerializer(people[offset]).data
    else:
        person = None

    assignment = {
        "date": day,
        "person": person
    }
    return assignment

def backup_assignment(day):
    """Returns the backup assignment (dict) for a given day"""
    day = datetime(year=day.year, month=day.month, day=day.day)
    people = Person.objects.filter(active=True)

    # Number of days since epoch, + 2
    if people:
        offset = (day - datetime(1970,1,2)).days % len(people)
        person = PersonSerializer(people[offset]).data
    else:
        person = None

    assignment = {
        "date": day,
        "person": person
    }
    return assignment
