from django.apps import AppConfig


class UptwinklesConfig(AppConfig):
    name = 'uptwinkles'
