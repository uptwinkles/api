from .models import Person, Chore, Meal
from rest_framework import serializers


class PersonSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Person
        fields = ('id', 'first_name', 'matrix_id', 'avatar_url', 'active')


class ChoreSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Chore
        fields = ('id', 'name', 'short_name', 'image')


class MealSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Meal
        fields = ('id', 'name')
