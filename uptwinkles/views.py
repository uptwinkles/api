import math
import time
from datetime import datetime, timedelta
from .models import Person, Chore, Meal
from rest_framework import viewsets, status
from rest_framework.response import Response
from .serializers import PersonSerializer, ChoreSerializer, MealSerializer
from rest_framework.decorators import list_route
from .helpers import chore_assignments, dinner_assignment, dishes_assignment, cleanup_assignment, backup_assignment


class PersonViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows people to be viewed or edited.
    """
    queryset = Person.objects.all()
    serializer_class = PersonSerializer
    filter_fields = ['active']


class ChoreViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows chores to be viewed or edited.
    """
    queryset = Chore.objects.all()
    serializer_class = ChoreSerializer


class MealViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows meals to be viewed or edited.
    """
    queryset = Meal.objects.all()
    serializer_class = MealSerializer


class ChoreAssignmentViewSet(viewsets.ViewSet):
    def list(self, request, format=None):
        assignments = [
            chore_assignments(datetime.now() - timedelta(days=7)),
            chore_assignments(datetime.now()),
            chore_assignments(datetime.now() + timedelta(days=7))
        ]
        return Response(assignments)

    def retrieve(self, request, pk=None):
        try:
            date = datetime.strptime(pk, "%Y-%m-%d")
        except ValueError:
            return Response(status=status.HTTP_404_NOT_FOUND)

        assignments = chore_assignments(date)
        return Response(assignments)


class DinnerAssignmentViewSet(viewsets.ViewSet):
    def list(self, request, format=None):
        assignments = [
            dinner_assignment(datetime.now() - timedelta(days=1)),
            dinner_assignment(datetime.now()),
            dinner_assignment(datetime.now() + timedelta(days=1)),
            dinner_assignment(datetime.now() + timedelta(days=2)),
            dinner_assignment(datetime.now() + timedelta(days=3)),
            dinner_assignment(datetime.now() + timedelta(days=4)),
            dinner_assignment(datetime.now() + timedelta(days=5)),
            dinner_assignment(datetime.now() + timedelta(days=6)),
            dinner_assignment(datetime.now() + timedelta(days=7))
        ]
        return Response(assignments)

    def retrieve(self, request, pk=None):
        try:
            date = datetime.strptime(pk, "%Y-%m-%d")
        except ValueError:
            return Response(status=status.HTTP_404_NOT_FOUND)

        assignment = dinner_assignment(date)
        return Response(assignment)


class DishesAssignmentViewSet(viewsets.ViewSet):
    def list(self, request, format=None):
        assignments = [
            dishes_assignment(datetime.now() - timedelta(days=1)),
            dishes_assignment(datetime.now()),
            dishes_assignment(datetime.now() + timedelta(days=1)),
            dishes_assignment(datetime.now() + timedelta(days=2)),
            dishes_assignment(datetime.now() + timedelta(days=3)),
            dishes_assignment(datetime.now() + timedelta(days=4)),
            dishes_assignment(datetime.now() + timedelta(days=5)),
            dishes_assignment(datetime.now() + timedelta(days=6)),
            dishes_assignment(datetime.now() + timedelta(days=7))
        ]
        return Response(assignments)

    def retrieve(self, request, pk=None):
        try:
            date = datetime.strptime(pk, "%Y-%m-%d")
        except ValueError:
            return Response(status=status.HTTP_404_NOT_FOUND)

        assignment = dishes_assignment(date)
        return Response(assignment)


class CleanupAssignmentViewSet(viewsets.ViewSet):
    def list(self, request, format=None):
        assignments = [
            cleanup_assignment(datetime.now() - timedelta(days=1)),
            cleanup_assignment(datetime.now()),
            cleanup_assignment(datetime.now() + timedelta(days=1)),
            cleanup_assignment(datetime.now() + timedelta(days=2)),
            cleanup_assignment(datetime.now() + timedelta(days=3)),
            cleanup_assignment(datetime.now() + timedelta(days=4)),
            cleanup_assignment(datetime.now() + timedelta(days=5)),
            cleanup_assignment(datetime.now() + timedelta(days=6)),
            cleanup_assignment(datetime.now() + timedelta(days=7))
        ]
        return Response(assignments)

    def retrieve(self, request, pk=None):
        try:
            date = datetime.strptime(pk, "%Y-%m-%d")
        except ValueError:
            return Response(status=status.HTTP_404_NOT_FOUND)

        assignment = cleanup_assignment(date)
        return Response(assignment)

class BackupAssignmentViewSet(viewsets.ViewSet):
    def list(self, request, format=None):
        assignments = [
            backup_assignment(datetime.now() - timedelta(days=1)),
            backup_assignment(datetime.now()),
            backup_assignment(datetime.now() + timedelta(days=1)),
            backup_assignment(datetime.now() + timedelta(days=2)),
            backup_assignment(datetime.now() + timedelta(days=3)),
            backup_assignment(datetime.now() + timedelta(days=4)),
            backup_assignment(datetime.now() + timedelta(days=5)),
            backup_assignment(datetime.now() + timedelta(days=6)),
            backup_assignment(datetime.now() + timedelta(days=7))
        ]
        return Response(assignments)

    def retrieve(self, request, pk=None):
        try:
            date = datetime.strptime(pk, "%Y-%m-%d")
        except ValueError:
            return Response(status=status.HTTP_404_NOT_FOUND)

        assignment = backup_assignment(date)
        return Response(assignment)
