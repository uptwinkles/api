from django.contrib import admin
from .models import Person, Chore, Meal

admin.site.register(Person)
admin.site.register(Chore)
admin.site.register(Meal)
